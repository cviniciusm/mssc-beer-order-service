package guru.sfg.beer.order.service.services.beer;

import java.util.Optional;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import guru.sfg.beer.order.service.config.ConfigProperties;
import guru.sfg.beer.order.service.web.model.BeerDto;

@Service
public class BeerServiceImpl implements BeerService {
	public final String BEER_PATH_V1 = "/api/v1/beer/";
	public final String BEER_UPC_PATH_V1 = "/api/v1/beerUpc/";
	private final RestTemplate restTemplate;

	private ConfigProperties configProperties;

	private String beerServiceHost;

	public BeerServiceImpl(RestTemplateBuilder restTemplateBuilder, ConfigProperties configProperties) {
		this.restTemplate = restTemplateBuilder.build();
		this.configProperties = configProperties;
	}

	@PostConstruct
	private void init() {
		beerServiceHost = configProperties.getBeerServiceHost();
	}

	@Override
	public Optional<BeerDto> getBeerById(UUID uuid) {
		return Optional.of(restTemplate.getForObject(beerServiceHost + BEER_PATH_V1 + uuid.toString(), BeerDto.class));
	}

	@Override
	public Optional<BeerDto> getBeerByUpc(String upc) {
		return Optional.of(restTemplate.getForObject(beerServiceHost + BEER_UPC_PATH_V1 + upc, BeerDto.class));
	}

}