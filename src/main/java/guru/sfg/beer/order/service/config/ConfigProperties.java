package guru.sfg.beer.order.service.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties(prefix = "sfg.brewery", ignoreUnknownFields = false)
@Getter
@Setter
public class ConfigProperties {
	private String beerServiceHost;
}
